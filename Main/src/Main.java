import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;

import org.antlr.v4.runtime.ANTLRInputStream;
import org.antlr.v4.runtime.Token;

public class Main {

	public static void main(String[] args) throws FileNotFoundException, IOException {
		
		mileLexer lexer = new mileLexer(new ANTLRInputStream(new FileInputStream("code.txt")));
		Token token = lexer.nextToken();
		
		while(token.getType() != Token.EOF) {
			System.out.println(
					token.getType() + ":" 
					+ token.getLine() + ":"
					+ token.getCharPositionInLine() + ":"
					+ token.getText());
			token = lexer.nextToken();
		}
	}

}
