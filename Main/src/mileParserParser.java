// Generated from mileParser.g4 by ANTLR 4.4
import org.antlr.v4.runtime.atn.*;
import org.antlr.v4.runtime.dfa.DFA;
import org.antlr.v4.runtime.*;
import org.antlr.v4.runtime.tree.*;
import java.util.List;

@SuppressWarnings({"all", "warnings", "unchecked", "unused", "cast"})
public class mileParserParser extends Parser {
	static { RuntimeMetaData.checkVersion("4.4", RuntimeMetaData.VERSION); }

	protected static final DFA[] _decisionToDFA;
	protected static final PredictionContextCache _sharedContextCache =
		new PredictionContextCache();
	public static final int
		PROGRAM_RW=1, PLUS=2, MINUS=3, TIMES=4, DIVISION=5, EXP=6, EQUAL=7, UPPER=8, 
		LOWER=9, TRUE_LITERAL=10, FALSE_LITERAL=11, AND_OPER=12, OR_OPER=13, DOT=14, 
		END_LINE=15, START_BLOCK=16, END_BLOCK=17, LPAR=18, RPAR=19, COMMENT=20, 
		PRINT=21, READ=22, UNARY=23, NUMBER=24, FLOAT=25, INT_TYPE=26, REAL_TYPE=27, 
		BOOL_TYPE=28, STRING_TYPE=29, STRUCT_TYPE=30, VOID_TYPE=31, VARIABLE_TYPE=32, 
		IF_RW=33, ELSE_RW=34, WHILE_RW=35, FOR_RW=36, BREAK_RW=37, CONTINUE_RW=38, 
		ID=39, STRING=40, WS=41;
	public static final String[] tokenNames = {
		"<INVALID>", "'arrancar'", "'+'", "'-'", "'*'", "'/'", "'**'", "'='", 
		"'>'", "'<'", "'tesla'", "'ferrari'", "'audi'", "'opel'", "'.'", "';'", 
		"'{'", "'}'", "'('", "')'", "COMMENT", "'porsche'", "'skoda'", "UNARY", 
		"NUMBER", "FLOAT", "'infiniti'", "'renault'", "'bugatti'", "'subaru'", 
		"'suzuki'", "'volvo'", "VARIABLE_TYPE", "'indian'", "'vespa'", "'lexus'", 
		"'ford'", "'bentley'", "'citroen'", "ID", "STRING", "WS"
	};
	public static final int
		RULE_s = 0, RULE_program = 1, RULE_stat = 2, RULE_operation = 3, RULE_atom = 4, 
		RULE_assing = 5, RULE_variable_type = 6, RULE_declaration = 7, RULE_condition = 8, 
		RULE_while_condition = 9, RULE_for_condition = 10, RULE_math_oper = 11, 
		RULE_bool_oper = 12, RULE_comparision = 13, RULE_print_block = 14;
	public static final String[] ruleNames = {
		"s", "program", "stat", "operation", "atom", "assing", "variable_type", 
		"declaration", "condition", "while_condition", "for_condition", "math_oper", 
		"bool_oper", "comparision", "print_block"
	};

	@Override
	public String getGrammarFileName() { return "mileParser.g4"; }

	@Override
	public String[] getTokenNames() { return tokenNames; }

	@Override
	public String[] getRuleNames() { return ruleNames; }

	@Override
	public String getSerializedATN() { return _serializedATN; }

	@Override
	public ATN getATN() { return _ATN; }

	public mileParserParser(TokenStream input) {
		super(input);
		_interp = new ParserATNSimulator(this,_ATN,_decisionToDFA,_sharedContextCache);
	}
	public static class SContext extends ParserRuleContext {
		public TerminalNode EOF() { return getToken(Recognizer.EOF, 0); }
		public OperationContext operation() {
			return getRuleContext(OperationContext.class,0);
		}
		public SContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_s; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof mileParserListener ) ((mileParserListener)listener).enterS(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof mileParserListener ) ((mileParserListener)listener).exitS(this);
		}
	}

	public final SContext s() throws RecognitionException {
		SContext _localctx = new SContext(_ctx, getState());
		enterRule(_localctx, 0, RULE_s);
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(30); operation(0);
			setState(31); match(EOF);
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class ProgramContext extends ParserRuleContext {
		public TerminalNode END_BLOCK() { return getToken(mileParserParser.END_BLOCK, 0); }
		public StatContext stat(int i) {
			return getRuleContext(StatContext.class,i);
		}
		public List<StatContext> stat() {
			return getRuleContexts(StatContext.class);
		}
		public TerminalNode START_BLOCK() { return getToken(mileParserParser.START_BLOCK, 0); }
		public TerminalNode PROGRAM_RW() { return getToken(mileParserParser.PROGRAM_RW, 0); }
		public ProgramContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_program; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof mileParserListener ) ((mileParserListener)listener).enterProgram(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof mileParserListener ) ((mileParserListener)listener).exitProgram(this);
		}
	}

	public final ProgramContext program() throws RecognitionException {
		ProgramContext _localctx = new ProgramContext(_ctx, getState());
		enterRule(_localctx, 2, RULE_program);
		int _la;
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(33); match(PROGRAM_RW);
			setState(34); match(START_BLOCK);
			setState(36); 
			_errHandler.sync(this);
			_la = _input.LA(1);
			do {
				{
				{
				setState(35); stat();
				}
				}
				setState(38); 
				_errHandler.sync(this);
				_la = _input.LA(1);
			} while ( (((_la) & ~0x3f) == 0 && ((1L << _la) & ((1L << PLUS) | (1L << MINUS) | (1L << LPAR) | (1L << PRINT) | (1L << NUMBER) | (1L << INT_TYPE) | (1L << REAL_TYPE) | (1L << BOOL_TYPE) | (1L << STRING_TYPE) | (1L << IF_RW) | (1L << WHILE_RW) | (1L << FOR_RW) | (1L << ID))) != 0) );
			setState(40); match(END_BLOCK);
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class StatContext extends ParserRuleContext {
		public AssingContext assing() {
			return getRuleContext(AssingContext.class,0);
		}
		public While_conditionContext while_condition() {
			return getRuleContext(While_conditionContext.class,0);
		}
		public SContext s() {
			return getRuleContext(SContext.class,0);
		}
		public For_conditionContext for_condition() {
			return getRuleContext(For_conditionContext.class,0);
		}
		public OperationContext operation() {
			return getRuleContext(OperationContext.class,0);
		}
		public DeclarationContext declaration() {
			return getRuleContext(DeclarationContext.class,0);
		}
		public Print_blockContext print_block() {
			return getRuleContext(Print_blockContext.class,0);
		}
		public ConditionContext condition() {
			return getRuleContext(ConditionContext.class,0);
		}
		public StatContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_stat; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof mileParserListener ) ((mileParserListener)listener).enterStat(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof mileParserListener ) ((mileParserListener)listener).exitStat(this);
		}
	}

	public final StatContext stat() throws RecognitionException {
		StatContext _localctx = new StatContext(_ctx, getState());
		enterRule(_localctx, 4, RULE_stat);
		try {
			setState(50);
			switch ( getInterpreter().adaptivePredict(_input,1,_ctx) ) {
			case 1:
				enterOuterAlt(_localctx, 1);
				{
				setState(42); assing();
				}
				break;
			case 2:
				enterOuterAlt(_localctx, 2);
				{
				setState(43); operation(0);
				}
				break;
			case 3:
				enterOuterAlt(_localctx, 3);
				{
				setState(44); declaration();
				}
				break;
			case 4:
				enterOuterAlt(_localctx, 4);
				{
				setState(45); print_block();
				}
				break;
			case 5:
				enterOuterAlt(_localctx, 5);
				{
				setState(46); s();
				}
				break;
			case 6:
				enterOuterAlt(_localctx, 6);
				{
				setState(47); condition();
				}
				break;
			case 7:
				enterOuterAlt(_localctx, 7);
				{
				setState(48); for_condition();
				}
				break;
			case 8:
				enterOuterAlt(_localctx, 8);
				{
				setState(49); while_condition();
				}
				break;
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class OperationContext extends ParserRuleContext {
		public TerminalNode ID() { return getToken(mileParserParser.ID, 0); }
		public OperationContext operation(int i) {
			return getRuleContext(OperationContext.class,i);
		}
		public TerminalNode PLUS() { return getToken(mileParserParser.PLUS, 0); }
		public TerminalNode MINUS() { return getToken(mileParserParser.MINUS, 0); }
		public TerminalNode LPAR() { return getToken(mileParserParser.LPAR, 0); }
		public List<OperationContext> operation() {
			return getRuleContexts(OperationContext.class);
		}
		public TerminalNode RPAR() { return getToken(mileParserParser.RPAR, 0); }
		public AtomContext atom() {
			return getRuleContext(AtomContext.class,0);
		}
		public OperationContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_operation; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof mileParserListener ) ((mileParserListener)listener).enterOperation(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof mileParserListener ) ((mileParserListener)listener).exitOperation(this);
		}
	}

	public final OperationContext operation() throws RecognitionException {
		return operation(0);
	}

	private OperationContext operation(int _p) throws RecognitionException {
		ParserRuleContext _parentctx = _ctx;
		int _parentState = getState();
		OperationContext _localctx = new OperationContext(_ctx, _parentState);
		OperationContext _prevctx = _localctx;
		int _startState = 6;
		enterRecursionRule(_localctx, 6, RULE_operation, _p);
		try {
			int _alt;
			enterOuterAlt(_localctx, 1);
			{
			setState(59);
			switch (_input.LA(1)) {
			case ID:
				{
				setState(53); match(ID);
				}
				break;
			case PLUS:
			case MINUS:
			case NUMBER:
				{
				setState(54); atom();
				}
				break;
			case LPAR:
				{
				setState(55); match(LPAR);
				setState(56); operation(0);
				setState(57); match(RPAR);
				}
				break;
			default:
				throw new NoViableAltException(this);
			}
			_ctx.stop = _input.LT(-1);
			setState(69);
			_errHandler.sync(this);
			_alt = getInterpreter().adaptivePredict(_input,4,_ctx);
			while ( _alt!=2 && _alt!=org.antlr.v4.runtime.atn.ATN.INVALID_ALT_NUMBER ) {
				if ( _alt==1 ) {
					if ( _parseListeners!=null ) triggerExitRuleEvent();
					_prevctx = _localctx;
					{
					setState(67);
					switch ( getInterpreter().adaptivePredict(_input,3,_ctx) ) {
					case 1:
						{
						_localctx = new OperationContext(_parentctx, _parentState);
						pushNewRecursionContext(_localctx, _startState, RULE_operation);
						setState(61);
						if (!(precpred(_ctx, 5))) throw new FailedPredicateException(this, "precpred(_ctx, 5)");
						setState(62); match(PLUS);
						setState(63); operation(6);
						}
						break;
					case 2:
						{
						_localctx = new OperationContext(_parentctx, _parentState);
						pushNewRecursionContext(_localctx, _startState, RULE_operation);
						setState(64);
						if (!(precpred(_ctx, 4))) throw new FailedPredicateException(this, "precpred(_ctx, 4)");
						setState(65); match(MINUS);
						setState(66); operation(5);
						}
						break;
					}
					} 
				}
				setState(71);
				_errHandler.sync(this);
				_alt = getInterpreter().adaptivePredict(_input,4,_ctx);
			}
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			unrollRecursionContexts(_parentctx);
		}
		return _localctx;
	}

	public static class AtomContext extends ParserRuleContext {
		public TerminalNode NUMBER() { return getToken(mileParserParser.NUMBER, 0); }
		public AtomContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_atom; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof mileParserListener ) ((mileParserListener)listener).enterAtom(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof mileParserListener ) ((mileParserListener)listener).exitAtom(this);
		}
	}

	public final AtomContext atom() throws RecognitionException {
		AtomContext _localctx = new AtomContext(_ctx, getState());
		enterRule(_localctx, 8, RULE_atom);
		int _la;
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(73);
			_la = _input.LA(1);
			if (_la==PLUS || _la==MINUS) {
				{
				setState(72);
				_la = _input.LA(1);
				if ( !(_la==PLUS || _la==MINUS) ) {
				_errHandler.recoverInline(this);
				}
				consume();
				}
			}

			setState(75); match(NUMBER);
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class AssingContext extends ParserRuleContext {
		public TerminalNode MINUS(int i) {
			return getToken(mileParserParser.MINUS, i);
		}
		public TerminalNode ID(int i) {
			return getToken(mileParserParser.ID, i);
		}
		public TerminalNode STRING() { return getToken(mileParserParser.STRING, 0); }
		public OperationContext operation() {
			return getRuleContext(OperationContext.class,0);
		}
		public TerminalNode PLUS(int i) {
			return getToken(mileParserParser.PLUS, i);
		}
		public TerminalNode FLOAT() { return getToken(mileParserParser.FLOAT, 0); }
		public List<TerminalNode> ID() { return getTokens(mileParserParser.ID); }
		public TerminalNode EQUAL() { return getToken(mileParserParser.EQUAL, 0); }
		public TerminalNode FALSE_LITERAL() { return getToken(mileParserParser.FALSE_LITERAL, 0); }
		public TerminalNode TRUE_LITERAL() { return getToken(mileParserParser.TRUE_LITERAL, 0); }
		public List<TerminalNode> PLUS() { return getTokens(mileParserParser.PLUS); }
		public List<TerminalNode> MINUS() { return getTokens(mileParserParser.MINUS); }
		public TerminalNode NUMBER() { return getToken(mileParserParser.NUMBER, 0); }
		public TerminalNode END_LINE() { return getToken(mileParserParser.END_LINE, 0); }
		public AssingContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_assing; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof mileParserListener ) ((mileParserListener)listener).enterAssing(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof mileParserListener ) ((mileParserListener)listener).exitAssing(this);
		}
	}

	public final AssingContext assing() throws RecognitionException {
		AssingContext _localctx = new AssingContext(_ctx, getState());
		enterRule(_localctx, 10, RULE_assing);
		try {
			setState(95);
			switch ( getInterpreter().adaptivePredict(_input,7,_ctx) ) {
			case 1:
				enterOuterAlt(_localctx, 1);
				{
				{
				setState(77); match(ID);
				setState(78); match(EQUAL);
				setState(86);
				switch ( getInterpreter().adaptivePredict(_input,6,_ctx) ) {
				case 1:
					{
					setState(79); operation(0);
					}
					break;
				case 2:
					{
					setState(80); match(ID);
					}
					break;
				case 3:
					{
					setState(81); match(NUMBER);
					}
					break;
				case 4:
					{
					setState(82); match(FLOAT);
					}
					break;
				case 5:
					{
					setState(83); match(STRING);
					}
					break;
				case 6:
					{
					setState(84); match(TRUE_LITERAL);
					}
					break;
				case 7:
					{
					setState(85); match(FALSE_LITERAL);
					}
					break;
				}
				setState(88); match(END_LINE);
				}
				}
				break;
			case 2:
				enterOuterAlt(_localctx, 2);
				{
				{
				setState(89); match(ID);
				setState(90); match(PLUS);
				setState(91); match(PLUS);
				}
				}
				break;
			case 3:
				enterOuterAlt(_localctx, 3);
				{
				{
				setState(92); match(ID);
				setState(93); match(MINUS);
				setState(94); match(MINUS);
				}
				}
				break;
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class Variable_typeContext extends ParserRuleContext {
		public TerminalNode REAL_TYPE() { return getToken(mileParserParser.REAL_TYPE, 0); }
		public TerminalNode INT_TYPE() { return getToken(mileParserParser.INT_TYPE, 0); }
		public TerminalNode STRING_TYPE() { return getToken(mileParserParser.STRING_TYPE, 0); }
		public TerminalNode BOOL_TYPE() { return getToken(mileParserParser.BOOL_TYPE, 0); }
		public Variable_typeContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_variable_type; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof mileParserListener ) ((mileParserListener)listener).enterVariable_type(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof mileParserListener ) ((mileParserListener)listener).exitVariable_type(this);
		}
	}

	public final Variable_typeContext variable_type() throws RecognitionException {
		Variable_typeContext _localctx = new Variable_typeContext(_ctx, getState());
		enterRule(_localctx, 12, RULE_variable_type);
		int _la;
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(97);
			_la = _input.LA(1);
			if ( !((((_la) & ~0x3f) == 0 && ((1L << _la) & ((1L << INT_TYPE) | (1L << REAL_TYPE) | (1L << BOOL_TYPE) | (1L << STRING_TYPE))) != 0)) ) {
			_errHandler.recoverInline(this);
			}
			consume();
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class DeclarationContext extends ParserRuleContext {
		public TerminalNode ID() { return getToken(mileParserParser.ID, 0); }
		public TerminalNode EQUAL() { return getToken(mileParserParser.EQUAL, 0); }
		public AssingContext assing() {
			return getRuleContext(AssingContext.class,0);
		}
		public Variable_typeContext variable_type() {
			return getRuleContext(Variable_typeContext.class,0);
		}
		public TerminalNode STRING() { return getToken(mileParserParser.STRING, 0); }
		public TerminalNode END_LINE() { return getToken(mileParserParser.END_LINE, 0); }
		public TerminalNode BOOL_TYPE() { return getToken(mileParserParser.BOOL_TYPE, 0); }
		public DeclarationContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_declaration; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof mileParserListener ) ((mileParserListener)listener).enterDeclaration(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof mileParserListener ) ((mileParserListener)listener).exitDeclaration(this);
		}
	}

	public final DeclarationContext declaration() throws RecognitionException {
		DeclarationContext _localctx = new DeclarationContext(_ctx, getState());
		enterRule(_localctx, 14, RULE_declaration);
		try {
			setState(122);
			switch ( getInterpreter().adaptivePredict(_input,8,_ctx) ) {
			case 1:
				enterOuterAlt(_localctx, 1);
				{
				{
				setState(99); variable_type();
				setState(100); match(ID);
				setState(101); match(END_LINE);
				}
				}
				break;
			case 2:
				enterOuterAlt(_localctx, 2);
				{
				{
				setState(103); variable_type();
				setState(104); assing();
				}
				}
				break;
			case 3:
				enterOuterAlt(_localctx, 3);
				{
				{
				setState(106); variable_type();
				setState(107); match(ID);
				setState(108); match(EQUAL);
				setState(109); match(STRING);
				setState(110); match(END_LINE);
				}
				}
				break;
			case 4:
				enterOuterAlt(_localctx, 4);
				{
				{
				setState(112); variable_type();
				setState(113); match(ID);
				setState(114); match(END_LINE);
				}
				}
				break;
			case 5:
				enterOuterAlt(_localctx, 5);
				{
				{
				setState(116); variable_type();
				setState(117); match(ID);
				setState(118); match(EQUAL);
				setState(119); match(BOOL_TYPE);
				setState(120); match(END_LINE);
				}
				}
				break;
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class ConditionContext extends ParserRuleContext {
		public ComparisionContext comparision() {
			return getRuleContext(ComparisionContext.class,0);
		}
		public TerminalNode END_BLOCK() { return getToken(mileParserParser.END_BLOCK, 0); }
		public StatContext stat(int i) {
			return getRuleContext(StatContext.class,i);
		}
		public List<StatContext> stat() {
			return getRuleContexts(StatContext.class);
		}
		public TerminalNode LPAR() { return getToken(mileParserParser.LPAR, 0); }
		public TerminalNode RPAR() { return getToken(mileParserParser.RPAR, 0); }
		public TerminalNode IF_RW() { return getToken(mileParserParser.IF_RW, 0); }
		public TerminalNode START_BLOCK() { return getToken(mileParserParser.START_BLOCK, 0); }
		public ConditionContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_condition; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof mileParserListener ) ((mileParserListener)listener).enterCondition(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof mileParserListener ) ((mileParserListener)listener).exitCondition(this);
		}
	}

	public final ConditionContext condition() throws RecognitionException {
		ConditionContext _localctx = new ConditionContext(_ctx, getState());
		enterRule(_localctx, 16, RULE_condition);
		int _la;
		try {
			setState(150);
			switch ( getInterpreter().adaptivePredict(_input,14,_ctx) ) {
			case 1:
				enterOuterAlt(_localctx, 1);
				{
				{
				setState(124); match(IF_RW);
				setState(126);
				_la = _input.LA(1);
				if (_la==LPAR) {
					{
					setState(125); match(LPAR);
					}
				}

				setState(128); comparision(0);
				setState(130);
				_la = _input.LA(1);
				if (_la==RPAR) {
					{
					setState(129); match(RPAR);
					}
				}

				setState(132); match(START_BLOCK);
				setState(134); 
				_errHandler.sync(this);
				_la = _input.LA(1);
				do {
					{
					{
					setState(133); stat();
					}
					}
					setState(136); 
					_errHandler.sync(this);
					_la = _input.LA(1);
				} while ( (((_la) & ~0x3f) == 0 && ((1L << _la) & ((1L << PLUS) | (1L << MINUS) | (1L << LPAR) | (1L << PRINT) | (1L << NUMBER) | (1L << INT_TYPE) | (1L << REAL_TYPE) | (1L << BOOL_TYPE) | (1L << STRING_TYPE) | (1L << IF_RW) | (1L << WHILE_RW) | (1L << FOR_RW) | (1L << ID))) != 0) );
				setState(138); match(END_BLOCK);
				}
				}
				break;
			case 2:
				enterOuterAlt(_localctx, 2);
				{
				{
				setState(140); match(IF_RW);
				setState(142);
				_la = _input.LA(1);
				if (_la==LPAR) {
					{
					setState(141); match(LPAR);
					}
				}

				setState(144); comparision(0);
				setState(146);
				_la = _input.LA(1);
				if (_la==RPAR) {
					{
					setState(145); match(RPAR);
					}
				}

				setState(148); stat();
				}
				}
				break;
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class While_conditionContext extends ParserRuleContext {
		public ComparisionContext comparision() {
			return getRuleContext(ComparisionContext.class,0);
		}
		public TerminalNode END_BLOCK() { return getToken(mileParserParser.END_BLOCK, 0); }
		public StatContext stat(int i) {
			return getRuleContext(StatContext.class,i);
		}
		public List<StatContext> stat() {
			return getRuleContexts(StatContext.class);
		}
		public TerminalNode WHILE_RW() { return getToken(mileParserParser.WHILE_RW, 0); }
		public TerminalNode LPAR() { return getToken(mileParserParser.LPAR, 0); }
		public TerminalNode RPAR() { return getToken(mileParserParser.RPAR, 0); }
		public TerminalNode START_BLOCK() { return getToken(mileParserParser.START_BLOCK, 0); }
		public While_conditionContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_while_condition; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof mileParserListener ) ((mileParserListener)listener).enterWhile_condition(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof mileParserListener ) ((mileParserListener)listener).exitWhile_condition(this);
		}
	}

	public final While_conditionContext while_condition() throws RecognitionException {
		While_conditionContext _localctx = new While_conditionContext(_ctx, getState());
		enterRule(_localctx, 18, RULE_while_condition);
		int _la;
		try {
			enterOuterAlt(_localctx, 1);
			{
			{
			setState(152); match(WHILE_RW);
			setState(154);
			_la = _input.LA(1);
			if (_la==LPAR) {
				{
				setState(153); match(LPAR);
				}
			}

			setState(156); comparision(0);
			setState(158);
			_la = _input.LA(1);
			if (_la==RPAR) {
				{
				setState(157); match(RPAR);
				}
			}

			setState(160); match(START_BLOCK);
			setState(162); 
			_errHandler.sync(this);
			_la = _input.LA(1);
			do {
				{
				{
				setState(161); stat();
				}
				}
				setState(164); 
				_errHandler.sync(this);
				_la = _input.LA(1);
			} while ( (((_la) & ~0x3f) == 0 && ((1L << _la) & ((1L << PLUS) | (1L << MINUS) | (1L << LPAR) | (1L << PRINT) | (1L << NUMBER) | (1L << INT_TYPE) | (1L << REAL_TYPE) | (1L << BOOL_TYPE) | (1L << STRING_TYPE) | (1L << IF_RW) | (1L << WHILE_RW) | (1L << FOR_RW) | (1L << ID))) != 0) );
			setState(166); match(END_BLOCK);
			}
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class For_conditionContext extends ParserRuleContext {
		public ComparisionContext comparision() {
			return getRuleContext(ComparisionContext.class,0);
		}
		public List<AssingContext> assing() {
			return getRuleContexts(AssingContext.class);
		}
		public AssingContext assing(int i) {
			return getRuleContext(AssingContext.class,i);
		}
		public TerminalNode FOR_RW() { return getToken(mileParserParser.FOR_RW, 0); }
		public TerminalNode END_BLOCK() { return getToken(mileParserParser.END_BLOCK, 0); }
		public StatContext stat(int i) {
			return getRuleContext(StatContext.class,i);
		}
		public List<StatContext> stat() {
			return getRuleContexts(StatContext.class);
		}
		public TerminalNode LPAR() { return getToken(mileParserParser.LPAR, 0); }
		public TerminalNode RPAR() { return getToken(mileParserParser.RPAR, 0); }
		public TerminalNode END_LINE() { return getToken(mileParserParser.END_LINE, 0); }
		public TerminalNode START_BLOCK() { return getToken(mileParserParser.START_BLOCK, 0); }
		public For_conditionContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_for_condition; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof mileParserListener ) ((mileParserListener)listener).enterFor_condition(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof mileParserListener ) ((mileParserListener)listener).exitFor_condition(this);
		}
	}

	public final For_conditionContext for_condition() throws RecognitionException {
		For_conditionContext _localctx = new For_conditionContext(_ctx, getState());
		enterRule(_localctx, 20, RULE_for_condition);
		int _la;
		try {
			enterOuterAlt(_localctx, 1);
			{
			{
			setState(168); match(FOR_RW);
			setState(169); match(LPAR);
			setState(170); assing();
			setState(171); comparision(0);
			setState(172); match(END_LINE);
			setState(173); assing();
			setState(174); match(RPAR);
			setState(175); match(START_BLOCK);
			setState(177); 
			_errHandler.sync(this);
			_la = _input.LA(1);
			do {
				{
				{
				setState(176); stat();
				}
				}
				setState(179); 
				_errHandler.sync(this);
				_la = _input.LA(1);
			} while ( (((_la) & ~0x3f) == 0 && ((1L << _la) & ((1L << PLUS) | (1L << MINUS) | (1L << LPAR) | (1L << PRINT) | (1L << NUMBER) | (1L << INT_TYPE) | (1L << REAL_TYPE) | (1L << BOOL_TYPE) | (1L << STRING_TYPE) | (1L << IF_RW) | (1L << WHILE_RW) | (1L << FOR_RW) | (1L << ID))) != 0) );
			setState(181); match(END_BLOCK);
			}
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class Math_operContext extends ParserRuleContext {
		public TerminalNode ID() { return getToken(mileParserParser.ID, 0); }
		public TerminalNode NUMBER() { return getToken(mileParserParser.NUMBER, 0); }
		public TerminalNode FLOAT() { return getToken(mileParserParser.FLOAT, 0); }
		public Math_operContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_math_oper; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof mileParserListener ) ((mileParserListener)listener).enterMath_oper(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof mileParserListener ) ((mileParserListener)listener).exitMath_oper(this);
		}
	}

	public final Math_operContext math_oper() throws RecognitionException {
		Math_operContext _localctx = new Math_operContext(_ctx, getState());
		enterRule(_localctx, 22, RULE_math_oper);
		int _la;
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(183);
			_la = _input.LA(1);
			if ( !((((_la) & ~0x3f) == 0 && ((1L << _la) & ((1L << NUMBER) | (1L << FLOAT) | (1L << ID))) != 0)) ) {
			_errHandler.recoverInline(this);
			}
			consume();
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class Bool_operContext extends ParserRuleContext {
		public TerminalNode ID() { return getToken(mileParserParser.ID, 0); }
		public TerminalNode FALSE_LITERAL() { return getToken(mileParserParser.FALSE_LITERAL, 0); }
		public TerminalNode TRUE_LITERAL() { return getToken(mileParserParser.TRUE_LITERAL, 0); }
		public Bool_operContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_bool_oper; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof mileParserListener ) ((mileParserListener)listener).enterBool_oper(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof mileParserListener ) ((mileParserListener)listener).exitBool_oper(this);
		}
	}

	public final Bool_operContext bool_oper() throws RecognitionException {
		Bool_operContext _localctx = new Bool_operContext(_ctx, getState());
		enterRule(_localctx, 24, RULE_bool_oper);
		int _la;
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(185);
			_la = _input.LA(1);
			if ( !((((_la) & ~0x3f) == 0 && ((1L << _la) & ((1L << TRUE_LITERAL) | (1L << FALSE_LITERAL) | (1L << ID))) != 0)) ) {
			_errHandler.recoverInline(this);
			}
			consume();
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class ComparisionContext extends ParserRuleContext {
		public List<TerminalNode> EQUAL() { return getTokens(mileParserParser.EQUAL); }
		public TerminalNode UPPER() { return getToken(mileParserParser.UPPER, 0); }
		public List<ComparisionContext> comparision() {
			return getRuleContexts(ComparisionContext.class);
		}
		public TerminalNode OR_OPER() { return getToken(mileParserParser.OR_OPER, 0); }
		public TerminalNode EQUAL(int i) {
			return getToken(mileParserParser.EQUAL, i);
		}
		public Math_operContext math_oper(int i) {
			return getRuleContext(Math_operContext.class,i);
		}
		public Bool_operContext bool_oper(int i) {
			return getRuleContext(Bool_operContext.class,i);
		}
		public TerminalNode LOWER() { return getToken(mileParserParser.LOWER, 0); }
		public ComparisionContext comparision(int i) {
			return getRuleContext(ComparisionContext.class,i);
		}
		public List<Math_operContext> math_oper() {
			return getRuleContexts(Math_operContext.class);
		}
		public List<Bool_operContext> bool_oper() {
			return getRuleContexts(Bool_operContext.class);
		}
		public TerminalNode AND_OPER() { return getToken(mileParserParser.AND_OPER, 0); }
		public ComparisionContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_comparision; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof mileParserListener ) ((mileParserListener)listener).enterComparision(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof mileParserListener ) ((mileParserListener)listener).exitComparision(this);
		}
	}

	public final ComparisionContext comparision() throws RecognitionException {
		return comparision(0);
	}

	private ComparisionContext comparision(int _p) throws RecognitionException {
		ParserRuleContext _parentctx = _ctx;
		int _parentState = getState();
		ComparisionContext _localctx = new ComparisionContext(_ctx, _parentState);
		ComparisionContext _prevctx = _localctx;
		int _startState = 26;
		enterRecursionRule(_localctx, 26, RULE_comparision, _p);
		int _la;
		try {
			int _alt;
			enterOuterAlt(_localctx, 1);
			{
			setState(212);
			switch ( getInterpreter().adaptivePredict(_input,21,_ctx) ) {
			case 1:
				{
				{
				setState(188); math_oper();
				setState(189); match(EQUAL);
				setState(190); match(EQUAL);
				setState(191); math_oper();
				}
				}
				break;
			case 2:
				{
				{
				setState(193); math_oper();
				setState(194); match(UPPER);
				setState(196);
				_la = _input.LA(1);
				if (_la==EQUAL) {
					{
					setState(195); match(EQUAL);
					}
				}

				setState(198); math_oper();
				}
				}
				break;
			case 3:
				{
				{
				setState(200); math_oper();
				setState(201); match(LOWER);
				setState(203);
				_la = _input.LA(1);
				if (_la==EQUAL) {
					{
					setState(202); match(EQUAL);
					}
				}

				setState(205); math_oper();
				}
				}
				break;
			case 4:
				{
				{
				setState(207); bool_oper();
				setState(208); match(EQUAL);
				setState(209); match(EQUAL);
				setState(210); bool_oper();
				}
				}
				break;
			}
			_ctx.stop = _input.LT(-1);
			setState(222);
			_errHandler.sync(this);
			_alt = getInterpreter().adaptivePredict(_input,23,_ctx);
			while ( _alt!=2 && _alt!=org.antlr.v4.runtime.atn.ATN.INVALID_ALT_NUMBER ) {
				if ( _alt==1 ) {
					if ( _parseListeners!=null ) triggerExitRuleEvent();
					_prevctx = _localctx;
					{
					setState(220);
					switch ( getInterpreter().adaptivePredict(_input,22,_ctx) ) {
					case 1:
						{
						_localctx = new ComparisionContext(_parentctx, _parentState);
						pushNewRecursionContext(_localctx, _startState, RULE_comparision);
						setState(214);
						if (!(precpred(_ctx, 6))) throw new FailedPredicateException(this, "precpred(_ctx, 6)");
						setState(215); match(AND_OPER);
						setState(216); comparision(7);
						}
						break;
					case 2:
						{
						_localctx = new ComparisionContext(_parentctx, _parentState);
						pushNewRecursionContext(_localctx, _startState, RULE_comparision);
						setState(217);
						if (!(precpred(_ctx, 5))) throw new FailedPredicateException(this, "precpred(_ctx, 5)");
						setState(218); match(OR_OPER);
						setState(219); comparision(6);
						}
						break;
					}
					} 
				}
				setState(224);
				_errHandler.sync(this);
				_alt = getInterpreter().adaptivePredict(_input,23,_ctx);
			}
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			unrollRecursionContexts(_parentctx);
		}
		return _localctx;
	}

	public static class Print_blockContext extends ParserRuleContext {
		public List<TerminalNode> ID() { return getTokens(mileParserParser.ID); }
		public TerminalNode PRINT() { return getToken(mileParserParser.PRINT, 0); }
		public TerminalNode STRING(int i) {
			return getToken(mileParserParser.STRING, i);
		}
		public List<TerminalNode> STRING() { return getTokens(mileParserParser.STRING); }
		public TerminalNode ID(int i) {
			return getToken(mileParserParser.ID, i);
		}
		public TerminalNode LPAR() { return getToken(mileParserParser.LPAR, 0); }
		public TerminalNode RPAR() { return getToken(mileParserParser.RPAR, 0); }
		public TerminalNode END_LINE() { return getToken(mileParserParser.END_LINE, 0); }
		public Print_blockContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_print_block; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof mileParserListener ) ((mileParserListener)listener).enterPrint_block(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof mileParserListener ) ((mileParserListener)listener).exitPrint_block(this);
		}
	}

	public final Print_blockContext print_block() throws RecognitionException {
		Print_blockContext _localctx = new Print_blockContext(_ctx, getState());
		enterRule(_localctx, 28, RULE_print_block);
		int _la;
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(225); match(PRINT);
			setState(226); match(LPAR);
			setState(228); 
			_errHandler.sync(this);
			_la = _input.LA(1);
			do {
				{
				{
				setState(227);
				_la = _input.LA(1);
				if ( !(_la==ID || _la==STRING) ) {
				_errHandler.recoverInline(this);
				}
				consume();
				}
				}
				setState(230); 
				_errHandler.sync(this);
				_la = _input.LA(1);
			} while ( _la==ID || _la==STRING );
			setState(232); match(RPAR);
			setState(233); match(END_LINE);
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	@Override
	public boolean sempred(RuleContext _localctx, int ruleIndex, int predIndex) {
		switch (ruleIndex) {
		case 3: return operation_sempred((OperationContext)_localctx, predIndex);
		case 13: return comparision_sempred((ComparisionContext)_localctx, predIndex);
		}
		return true;
	}
	private boolean comparision_sempred(ComparisionContext _localctx, int predIndex) {
		switch (predIndex) {
		case 2: return precpred(_ctx, 6);
		case 3: return precpred(_ctx, 5);
		}
		return true;
	}
	private boolean operation_sempred(OperationContext _localctx, int predIndex) {
		switch (predIndex) {
		case 0: return precpred(_ctx, 5);
		case 1: return precpred(_ctx, 4);
		}
		return true;
	}

	public static final String _serializedATN =
		"\3\u0430\ud6d1\u8206\uad2d\u4417\uaef1\u8d80\uaadd\3+\u00ee\4\2\t\2\4"+
		"\3\t\3\4\4\t\4\4\5\t\5\4\6\t\6\4\7\t\7\4\b\t\b\4\t\t\t\4\n\t\n\4\13\t"+
		"\13\4\f\t\f\4\r\t\r\4\16\t\16\4\17\t\17\4\20\t\20\3\2\3\2\3\2\3\3\3\3"+
		"\3\3\6\3\'\n\3\r\3\16\3(\3\3\3\3\3\4\3\4\3\4\3\4\3\4\3\4\3\4\3\4\5\4\65"+
		"\n\4\3\5\3\5\3\5\3\5\3\5\3\5\3\5\5\5>\n\5\3\5\3\5\3\5\3\5\3\5\3\5\7\5"+
		"F\n\5\f\5\16\5I\13\5\3\6\5\6L\n\6\3\6\3\6\3\7\3\7\3\7\3\7\3\7\3\7\3\7"+
		"\3\7\3\7\5\7Y\n\7\3\7\3\7\3\7\3\7\3\7\3\7\3\7\5\7b\n\7\3\b\3\b\3\t\3\t"+
		"\3\t\3\t\3\t\3\t\3\t\3\t\3\t\3\t\3\t\3\t\3\t\3\t\3\t\3\t\3\t\3\t\3\t\3"+
		"\t\3\t\3\t\3\t\5\t}\n\t\3\n\3\n\5\n\u0081\n\n\3\n\3\n\5\n\u0085\n\n\3"+
		"\n\3\n\6\n\u0089\n\n\r\n\16\n\u008a\3\n\3\n\3\n\3\n\5\n\u0091\n\n\3\n"+
		"\3\n\5\n\u0095\n\n\3\n\3\n\5\n\u0099\n\n\3\13\3\13\5\13\u009d\n\13\3\13"+
		"\3\13\5\13\u00a1\n\13\3\13\3\13\6\13\u00a5\n\13\r\13\16\13\u00a6\3\13"+
		"\3\13\3\f\3\f\3\f\3\f\3\f\3\f\3\f\3\f\3\f\6\f\u00b4\n\f\r\f\16\f\u00b5"+
		"\3\f\3\f\3\r\3\r\3\16\3\16\3\17\3\17\3\17\3\17\3\17\3\17\3\17\3\17\3\17"+
		"\5\17\u00c7\n\17\3\17\3\17\3\17\3\17\3\17\5\17\u00ce\n\17\3\17\3\17\3"+
		"\17\3\17\3\17\3\17\3\17\5\17\u00d7\n\17\3\17\3\17\3\17\3\17\3\17\3\17"+
		"\7\17\u00df\n\17\f\17\16\17\u00e2\13\17\3\20\3\20\3\20\6\20\u00e7\n\20"+
		"\r\20\16\20\u00e8\3\20\3\20\3\20\3\20\2\4\b\34\21\2\4\6\b\n\f\16\20\22"+
		"\24\26\30\32\34\36\2\7\3\2\4\5\3\2\34\37\4\2\32\33))\4\2\f\r))\3\2)*\u0109"+
		"\2 \3\2\2\2\4#\3\2\2\2\6\64\3\2\2\2\b=\3\2\2\2\nK\3\2\2\2\fa\3\2\2\2\16"+
		"c\3\2\2\2\20|\3\2\2\2\22\u0098\3\2\2\2\24\u009a\3\2\2\2\26\u00aa\3\2\2"+
		"\2\30\u00b9\3\2\2\2\32\u00bb\3\2\2\2\34\u00d6\3\2\2\2\36\u00e3\3\2\2\2"+
		" !\5\b\5\2!\"\7\2\2\3\"\3\3\2\2\2#$\7\3\2\2$&\7\22\2\2%\'\5\6\4\2&%\3"+
		"\2\2\2\'(\3\2\2\2(&\3\2\2\2()\3\2\2\2)*\3\2\2\2*+\7\23\2\2+\5\3\2\2\2"+
		",\65\5\f\7\2-\65\5\b\5\2.\65\5\20\t\2/\65\5\36\20\2\60\65\5\2\2\2\61\65"+
		"\5\22\n\2\62\65\5\26\f\2\63\65\5\24\13\2\64,\3\2\2\2\64-\3\2\2\2\64.\3"+
		"\2\2\2\64/\3\2\2\2\64\60\3\2\2\2\64\61\3\2\2\2\64\62\3\2\2\2\64\63\3\2"+
		"\2\2\65\7\3\2\2\2\66\67\b\5\1\2\67>\7)\2\28>\5\n\6\29:\7\24\2\2:;\5\b"+
		"\5\2;<\7\25\2\2<>\3\2\2\2=\66\3\2\2\2=8\3\2\2\2=9\3\2\2\2>G\3\2\2\2?@"+
		"\f\7\2\2@A\7\4\2\2AF\5\b\5\bBC\f\6\2\2CD\7\5\2\2DF\5\b\5\7E?\3\2\2\2E"+
		"B\3\2\2\2FI\3\2\2\2GE\3\2\2\2GH\3\2\2\2H\t\3\2\2\2IG\3\2\2\2JL\t\2\2\2"+
		"KJ\3\2\2\2KL\3\2\2\2LM\3\2\2\2MN\7\32\2\2N\13\3\2\2\2OP\7)\2\2PX\7\t\2"+
		"\2QY\5\b\5\2RY\7)\2\2SY\7\32\2\2TY\7\33\2\2UY\7*\2\2VY\7\f\2\2WY\7\r\2"+
		"\2XQ\3\2\2\2XR\3\2\2\2XS\3\2\2\2XT\3\2\2\2XU\3\2\2\2XV\3\2\2\2XW\3\2\2"+
		"\2YZ\3\2\2\2Zb\7\21\2\2[\\\7)\2\2\\]\7\4\2\2]b\7\4\2\2^_\7)\2\2_`\7\5"+
		"\2\2`b\7\5\2\2aO\3\2\2\2a[\3\2\2\2a^\3\2\2\2b\r\3\2\2\2cd\t\3\2\2d\17"+
		"\3\2\2\2ef\5\16\b\2fg\7)\2\2gh\7\21\2\2h}\3\2\2\2ij\5\16\b\2jk\5\f\7\2"+
		"k}\3\2\2\2lm\5\16\b\2mn\7)\2\2no\7\t\2\2op\7*\2\2pq\7\21\2\2q}\3\2\2\2"+
		"rs\5\16\b\2st\7)\2\2tu\7\21\2\2u}\3\2\2\2vw\5\16\b\2wx\7)\2\2xy\7\t\2"+
		"\2yz\7\36\2\2z{\7\21\2\2{}\3\2\2\2|e\3\2\2\2|i\3\2\2\2|l\3\2\2\2|r\3\2"+
		"\2\2|v\3\2\2\2}\21\3\2\2\2~\u0080\7#\2\2\177\u0081\7\24\2\2\u0080\177"+
		"\3\2\2\2\u0080\u0081\3\2\2\2\u0081\u0082\3\2\2\2\u0082\u0084\5\34\17\2"+
		"\u0083\u0085\7\25\2\2\u0084\u0083\3\2\2\2\u0084\u0085\3\2\2\2\u0085\u0086"+
		"\3\2\2\2\u0086\u0088\7\22\2\2\u0087\u0089\5\6\4\2\u0088\u0087\3\2\2\2"+
		"\u0089\u008a\3\2\2\2\u008a\u0088\3\2\2\2\u008a\u008b\3\2\2\2\u008b\u008c"+
		"\3\2\2\2\u008c\u008d\7\23\2\2\u008d\u0099\3\2\2\2\u008e\u0090\7#\2\2\u008f"+
		"\u0091\7\24\2\2\u0090\u008f\3\2\2\2\u0090\u0091\3\2\2\2\u0091\u0092\3"+
		"\2\2\2\u0092\u0094\5\34\17\2\u0093\u0095\7\25\2\2\u0094\u0093\3\2\2\2"+
		"\u0094\u0095\3\2\2\2\u0095\u0096\3\2\2\2\u0096\u0097\5\6\4\2\u0097\u0099"+
		"\3\2\2\2\u0098~\3\2\2\2\u0098\u008e\3\2\2\2\u0099\23\3\2\2\2\u009a\u009c"+
		"\7%\2\2\u009b\u009d\7\24\2\2\u009c\u009b\3\2\2\2\u009c\u009d\3\2\2\2\u009d"+
		"\u009e\3\2\2\2\u009e\u00a0\5\34\17\2\u009f\u00a1\7\25\2\2\u00a0\u009f"+
		"\3\2\2\2\u00a0\u00a1\3\2\2\2\u00a1\u00a2\3\2\2\2\u00a2\u00a4\7\22\2\2"+
		"\u00a3\u00a5\5\6\4\2\u00a4\u00a3\3\2\2\2\u00a5\u00a6\3\2\2\2\u00a6\u00a4"+
		"\3\2\2\2\u00a6\u00a7\3\2\2\2\u00a7\u00a8\3\2\2\2\u00a8\u00a9\7\23\2\2"+
		"\u00a9\25\3\2\2\2\u00aa\u00ab\7&\2\2\u00ab\u00ac\7\24\2\2\u00ac\u00ad"+
		"\5\f\7\2\u00ad\u00ae\5\34\17\2\u00ae\u00af\7\21\2\2\u00af\u00b0\5\f\7"+
		"\2\u00b0\u00b1\7\25\2\2\u00b1\u00b3\7\22\2\2\u00b2\u00b4\5\6\4\2\u00b3"+
		"\u00b2\3\2\2\2\u00b4\u00b5\3\2\2\2\u00b5\u00b3\3\2\2\2\u00b5\u00b6\3\2"+
		"\2\2\u00b6\u00b7\3\2\2\2\u00b7\u00b8\7\23\2\2\u00b8\27\3\2\2\2\u00b9\u00ba"+
		"\t\4\2\2\u00ba\31\3\2\2\2\u00bb\u00bc\t\5\2\2\u00bc\33\3\2\2\2\u00bd\u00be"+
		"\b\17\1\2\u00be\u00bf\5\30\r\2\u00bf\u00c0\7\t\2\2\u00c0\u00c1\7\t\2\2"+
		"\u00c1\u00c2\5\30\r\2\u00c2\u00d7\3\2\2\2\u00c3\u00c4\5\30\r\2\u00c4\u00c6"+
		"\7\n\2\2\u00c5\u00c7\7\t\2\2\u00c6\u00c5\3\2\2\2\u00c6\u00c7\3\2\2\2\u00c7"+
		"\u00c8\3\2\2\2\u00c8\u00c9\5\30\r\2\u00c9\u00d7\3\2\2\2\u00ca\u00cb\5"+
		"\30\r\2\u00cb\u00cd\7\13\2\2\u00cc\u00ce\7\t\2\2\u00cd\u00cc\3\2\2\2\u00cd"+
		"\u00ce\3\2\2\2\u00ce\u00cf\3\2\2\2\u00cf\u00d0\5\30\r\2\u00d0\u00d7\3"+
		"\2\2\2\u00d1\u00d2\5\32\16\2\u00d2\u00d3\7\t\2\2\u00d3\u00d4\7\t\2\2\u00d4"+
		"\u00d5\5\32\16\2\u00d5\u00d7\3\2\2\2\u00d6\u00bd\3\2\2\2\u00d6\u00c3\3"+
		"\2\2\2\u00d6\u00ca\3\2\2\2\u00d6\u00d1\3\2\2\2\u00d7\u00e0\3\2\2\2\u00d8"+
		"\u00d9\f\b\2\2\u00d9\u00da\7\16\2\2\u00da\u00df\5\34\17\t\u00db\u00dc"+
		"\f\7\2\2\u00dc\u00dd\7\17\2\2\u00dd\u00df\5\34\17\b\u00de\u00d8\3\2\2"+
		"\2\u00de\u00db\3\2\2\2\u00df\u00e2\3\2\2\2\u00e0\u00de\3\2\2\2\u00e0\u00e1"+
		"\3\2\2\2\u00e1\35\3\2\2\2\u00e2\u00e0\3\2\2\2\u00e3\u00e4\7\27\2\2\u00e4"+
		"\u00e6\7\24\2\2\u00e5\u00e7\t\6\2\2\u00e6\u00e5\3\2\2\2\u00e7\u00e8\3"+
		"\2\2\2\u00e8\u00e6\3\2\2\2\u00e8\u00e9\3\2\2\2\u00e9\u00ea\3\2\2\2\u00ea"+
		"\u00eb\7\25\2\2\u00eb\u00ec\7\21\2\2\u00ec\37\3\2\2\2\33(\64=EGKXa|\u0080"+
		"\u0084\u008a\u0090\u0094\u0098\u009c\u00a0\u00a6\u00b5\u00c6\u00cd\u00d6"+
		"\u00de\u00e0\u00e8";
	public static final ATN _ATN =
		new ATNDeserializer().deserialize(_serializedATN.toCharArray());
	static {
		_decisionToDFA = new DFA[_ATN.getNumberOfDecisions()];
		for (int i = 0; i < _ATN.getNumberOfDecisions(); i++) {
			_decisionToDFA[i] = new DFA(_ATN.getDecisionState(i), i);
		}
	}
}