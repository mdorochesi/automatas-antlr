// Generated from mileParser.g4 by ANTLR 4.4
import org.antlr.v4.runtime.misc.NotNull;
import org.antlr.v4.runtime.tree.ParseTreeListener;

/**
 * This interface defines a complete listener for a parse tree produced by
 * {@link mileParserParser}.
 */
public interface mileParserListener extends ParseTreeListener {
	/**
	 * Enter a parse tree produced by {@link mileParserParser#while_condition}.
	 * @param ctx the parse tree
	 */
	void enterWhile_condition(@NotNull mileParserParser.While_conditionContext ctx);
	/**
	 * Exit a parse tree produced by {@link mileParserParser#while_condition}.
	 * @param ctx the parse tree
	 */
	void exitWhile_condition(@NotNull mileParserParser.While_conditionContext ctx);
	/**
	 * Enter a parse tree produced by {@link mileParserParser#bool_oper}.
	 * @param ctx the parse tree
	 */
	void enterBool_oper(@NotNull mileParserParser.Bool_operContext ctx);
	/**
	 * Exit a parse tree produced by {@link mileParserParser#bool_oper}.
	 * @param ctx the parse tree
	 */
	void exitBool_oper(@NotNull mileParserParser.Bool_operContext ctx);
	/**
	 * Enter a parse tree produced by {@link mileParserParser#stat}.
	 * @param ctx the parse tree
	 */
	void enterStat(@NotNull mileParserParser.StatContext ctx);
	/**
	 * Exit a parse tree produced by {@link mileParserParser#stat}.
	 * @param ctx the parse tree
	 */
	void exitStat(@NotNull mileParserParser.StatContext ctx);
	/**
	 * Enter a parse tree produced by {@link mileParserParser#variable_type}.
	 * @param ctx the parse tree
	 */
	void enterVariable_type(@NotNull mileParserParser.Variable_typeContext ctx);
	/**
	 * Exit a parse tree produced by {@link mileParserParser#variable_type}.
	 * @param ctx the parse tree
	 */
	void exitVariable_type(@NotNull mileParserParser.Variable_typeContext ctx);
	/**
	 * Enter a parse tree produced by {@link mileParserParser#print_block}.
	 * @param ctx the parse tree
	 */
	void enterPrint_block(@NotNull mileParserParser.Print_blockContext ctx);
	/**
	 * Exit a parse tree produced by {@link mileParserParser#print_block}.
	 * @param ctx the parse tree
	 */
	void exitPrint_block(@NotNull mileParserParser.Print_blockContext ctx);
	/**
	 * Enter a parse tree produced by {@link mileParserParser#program}.
	 * @param ctx the parse tree
	 */
	void enterProgram(@NotNull mileParserParser.ProgramContext ctx);
	/**
	 * Exit a parse tree produced by {@link mileParserParser#program}.
	 * @param ctx the parse tree
	 */
	void exitProgram(@NotNull mileParserParser.ProgramContext ctx);
	/**
	 * Enter a parse tree produced by {@link mileParserParser#math_oper}.
	 * @param ctx the parse tree
	 */
	void enterMath_oper(@NotNull mileParserParser.Math_operContext ctx);
	/**
	 * Exit a parse tree produced by {@link mileParserParser#math_oper}.
	 * @param ctx the parse tree
	 */
	void exitMath_oper(@NotNull mileParserParser.Math_operContext ctx);
	/**
	 * Enter a parse tree produced by {@link mileParserParser#comparision}.
	 * @param ctx the parse tree
	 */
	void enterComparision(@NotNull mileParserParser.ComparisionContext ctx);
	/**
	 * Exit a parse tree produced by {@link mileParserParser#comparision}.
	 * @param ctx the parse tree
	 */
	void exitComparision(@NotNull mileParserParser.ComparisionContext ctx);
	/**
	 * Enter a parse tree produced by {@link mileParserParser#declaration}.
	 * @param ctx the parse tree
	 */
	void enterDeclaration(@NotNull mileParserParser.DeclarationContext ctx);
	/**
	 * Exit a parse tree produced by {@link mileParserParser#declaration}.
	 * @param ctx the parse tree
	 */
	void exitDeclaration(@NotNull mileParserParser.DeclarationContext ctx);
	/**
	 * Enter a parse tree produced by {@link mileParserParser#assing}.
	 * @param ctx the parse tree
	 */
	void enterAssing(@NotNull mileParserParser.AssingContext ctx);
	/**
	 * Exit a parse tree produced by {@link mileParserParser#assing}.
	 * @param ctx the parse tree
	 */
	void exitAssing(@NotNull mileParserParser.AssingContext ctx);
	/**
	 * Enter a parse tree produced by {@link mileParserParser#condition}.
	 * @param ctx the parse tree
	 */
	void enterCondition(@NotNull mileParserParser.ConditionContext ctx);
	/**
	 * Exit a parse tree produced by {@link mileParserParser#condition}.
	 * @param ctx the parse tree
	 */
	void exitCondition(@NotNull mileParserParser.ConditionContext ctx);
	/**
	 * Enter a parse tree produced by {@link mileParserParser#s}.
	 * @param ctx the parse tree
	 */
	void enterS(@NotNull mileParserParser.SContext ctx);
	/**
	 * Exit a parse tree produced by {@link mileParserParser#s}.
	 * @param ctx the parse tree
	 */
	void exitS(@NotNull mileParserParser.SContext ctx);
	/**
	 * Enter a parse tree produced by {@link mileParserParser#for_condition}.
	 * @param ctx the parse tree
	 */
	void enterFor_condition(@NotNull mileParserParser.For_conditionContext ctx);
	/**
	 * Exit a parse tree produced by {@link mileParserParser#for_condition}.
	 * @param ctx the parse tree
	 */
	void exitFor_condition(@NotNull mileParserParser.For_conditionContext ctx);
	/**
	 * Enter a parse tree produced by {@link mileParserParser#atom}.
	 * @param ctx the parse tree
	 */
	void enterAtom(@NotNull mileParserParser.AtomContext ctx);
	/**
	 * Exit a parse tree produced by {@link mileParserParser#atom}.
	 * @param ctx the parse tree
	 */
	void exitAtom(@NotNull mileParserParser.AtomContext ctx);
	/**
	 * Enter a parse tree produced by {@link mileParserParser#operation}.
	 * @param ctx the parse tree
	 */
	void enterOperation(@NotNull mileParserParser.OperationContext ctx);
	/**
	 * Exit a parse tree produced by {@link mileParserParser#operation}.
	 * @param ctx the parse tree
	 */
	void exitOperation(@NotNull mileParserParser.OperationContext ctx);
}