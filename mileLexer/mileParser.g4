/**
 * Define a grammar called Hello
 */
grammar mileParser;
import mileLexer;

s : operation EOF ;


program: PROGRAM_RW START_BLOCK 
			stat+
		END_BLOCK;



stat : assing
    |  operation
    |  declaration
    |  print_block
    |  s
    | condition
    | for_condition
    | while_condition
    | iter
    ;
    
iter : (BREAK_RW | CONTINUE_RW) END_LINE;
    
operation : operation PLUS operation 
			| operation MINUS operation
			| ID
			| atom
			| LPAR operation RPAR
			;

atom : ('-'|'+')? NUMBER;

assing : (ID EQUAL (operation | ID | NUMBER | FLOAT | STRING | TRUE_LITERAL | FALSE_LITERAL) END_LINE)|(ID PLUS PLUS)|(ID MINUS MINUS);

variable_type: INT_TYPE | STRING_TYPE | BOOL_TYPE | REAL_TYPE;

declaration : (variable_type ID END_LINE) | (variable_type assing) 
			| (variable_type ID EQUAL STRING END_LINE) 
			| (variable_type ID END_LINE) | (variable_type ID EQUAL (TRUE_LITERAL | FALSE_LITERAL) END_LINE);
			
condition : (IF_RW LPAR? comparision RPAR? START_BLOCK stat+ END_BLOCK)|(IF_RW LPAR? comparision RPAR? stat);

while_condition: (WHILE_RW LPAR? comparision RPAR? START_BLOCK stat+ END_BLOCK);

for_condition: (FOR_RW LPAR assing comparision END_LINE assing RPAR START_BLOCK stat+ END_BLOCK);

math_oper: ID | NUMBER | FLOAT;
bool_oper: ID | TRUE_LITERAL | FALSE_LITERAL; 

comparision :  comparision AND_OPER  comparision
			 | comparision  OR_OPER  comparision
			 | (math_oper EQUAL EQUAL math_oper)
			 | (math_oper UPPER EQUAL? math_oper)
			 | (math_oper LOWER EQUAL? math_oper)
			 | (bool_oper EQUAL EQUAL bool_oper);

  
print_block: PRINT LPAR (STRING|ID)+ RPAR END_LINE ;