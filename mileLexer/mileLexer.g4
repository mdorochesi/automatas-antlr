/**
 * Define a grammar called Hello
 */
lexer grammar mileLexer;

PROGRAM_RW: 'arrancar'; /* Inicio de programa */
/*
 * Operadores
 */

PLUS : '+'; /* Suma */

MINUS : '-'; /* Resta */

TIMES : '*'; /* Multiplicacion */

DIVISION : '/'; /* Division */
//
EXP : '**'; /* Exponencial */

EQUAL : '='; /* Igualacion */

UPPER : '>'; /* Mayor */

LOWER : '<'; /* Menor */

TRUE_LITERAL: 'tesla'; 

FALSE_LITERAL: 'ferrari';

AND_OPER: 'audi';

OR_OPER: 'opel';

/*
 * Caracteres Especiales
 */

DOT : '.'; /* Punto */

END_LINE : ';'; /* Punto y Coma */

START_BLOCK : '{'; /* Inicio de un bloque */

END_BLOCK : '}'; /* Fin de un bloque */

LPAR : '('; /* Parentesis izquierdo */

RPAR : ')'; /* Parentesis derecho */

/*
 * structures
 */

COMMENT: '#' .*?  '\r' ? '\n' -> skip;

/*
 * Funciones
 */
 
 PRINT : 'porsche';
 READ : 'skoda';

//

UNARY : '-'|'+';
fragment NAT : [0-9];
NUMBER :  NAT+;
FLOAT : NAT+ DOT NAT+;

/* 
 * Tipos
 */

INT_TYPE: 'infiniti';
REAL_TYPE: 'renault';
BOOL_TYPE: 'bugatti';
STRING_TYPE: 'subaru';
STRUCT_TYPE : 'suzuki';
VOID_TYPE : 'volvo';

VARIABLE_TYPE : INT_TYPE | REAL_TYPE | BOOL_TYPE | STRING_TYPE | VOID_TYPE;

/*
 * Estructuras Selectivas e Iterativas
 */
IF_RW: 'indian';
ELSE_RW: 'vespa';
WHILE_RW: 'lexus';

FOR_RW: 'ford';
BREAK_RW : 'frenar';
CONTINUE_RW : 'acelerar';


//
ID : [a-z]+ ;             // match lower-case identifiers

STRING: '"' .* ? '"' ;

WS : [ \t\r\n]+ -> skip ; // skip spaces, tabs, newlines
